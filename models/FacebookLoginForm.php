<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03.06.18
 * Time: 18:08
 */

namespace app\models;

use yii\base\Model;
use splynx\helpers\ApiHelper;
use splynx\models\Customer;

class FacebookLoginForm extends Model
{
    public $email;
    public $name;
    public $password;
    public $password_repeat;
    public $street;
    public $zip;
    public $city;
    public $fbUserId;

    public function rules()
    {
        return [
            [['email', 'name', 'password', 'password_repeat', 'street', 'zip', 'city', 'fbUserId'], 'required'],
            [['password', 'password_repeat', 'street', 'zip', 'city'], 'string', 'min' => 1, 'max' => 255],
            ['fbUserId', 'integer'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'             => \Yii::t('app', 'Email'),
            'name'              => \Yii::t('app', 'Full name'),
            'password'          => \Yii::t('app', 'Password'),
            'password_repeat'   => \Yii::t('app', 'Password repeat'),
            'address'           => \Yii::t('app', 'Address'),
        ];
    }

    /**
     * Return customer ID or null
     */
    public function addNewCustomer()
    {
        $result = ApiHelper::getInstance()->post('admin/customers/customer', [
            'status'        => 'active',
            'partner_id'    => 1,
            'location_id'   => 1,
            'name'          => $this->name,
            'email'         => $this->email,
            'category'      => 'person',
            'street_1'      => $this->street,
            'zip_code'      => $this->zip,
            'city'          => $this->city,
        ]);

        if ($result['result'] == false or empty($result['response'])) {
            return null;
        }

        // Save FB user id in database
        $customersFbLogin = new CustomersFbLogin();
        $customersFbLogin->facebook_user_id = $this->fbUserId;
        $customersFbLogin->customer_id = $result['response']['id'];
        $customersFbLogin->save();

        //Send Confirm email
        $emailResponse = ApiHelper::getInstance()->post('admin/config/mail', [
            'type'          => 'message',
            'status'        => 'sent',
            'recipient'     => $this->email,
            'subject'       => 'Account has been created',
            'message'       => 'Thank you! Your account was successfully created.'
        ]);

        return $result['response']['id'];
    }

    public function login($customerId)
    {
        return \Yii::$app->user->login(Customer::findIdentity($customerId), 3600 * 24 * 30);
    }
}