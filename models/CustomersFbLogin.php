<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03.06.18
 * Time: 20:58
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class CustomersFbLogin
 * @property int facebook_user_id
 * @property int customer_id
 * @package app\models
 */
class CustomersFbLogin extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%customers_fb_login}}';
    }

    public function rules()
    {
        return [
            [['customer_id', 'facebook_user_id'], 'required'],
            [['customer_id', 'facebook_user_id'], 'integer'],
        ];
    }
}