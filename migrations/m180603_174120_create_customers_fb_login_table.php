<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customers_fb_login`.
 */
class m180603_174120_create_customers_fb_login_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customers_fb_login', [
            'customer_id'       => $this->integer()->unique()->unsigned(),
            'facebook_user_id'  => $this->integer()->unique()->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('customers_fb_login');
    }
}
