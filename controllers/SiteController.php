<?php

namespace app\controllers;

use app\models\FacebookLoginForm;
use app\models\CustomersFbLogin;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'fb-callback', 'fb-sign-up'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'fb-callback', 'fb-sign-up'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Homepage for FB login
     *
     * @return string
     * @throws FacebookSDKException
     */
    public function actionIndex()
    {
        \Yii::$app->session->open();
        $this->getView()->title = 'Splynx Add-on Login via Facebook';

        $fb = $this->getFbInstance();
        $helper = $fb->getRedirectLoginHelper();
        $loginUrl = $helper->getLoginUrl(Url::toRoute(['site/fb-callback'], true), ['email']);

        return $this->render('index', [
            'fbLoginUrl'    =>  $loginUrl,
        ]);
    }

    /**
     * Getting access token from User to FB login
     *
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     * @throws FacebookSDKException
     */
    public function actionFbCallback()
    {
        \Yii::$app->session->open();
        $fb = $this->getFbInstance();
        $helper = $fb->getRedirectLoginHelper();
        try {
            $accessToken = $helper->getAccessToken();
            if (!$accessToken) {
                return $this->redirect(['site/index']);
            }

            \Yii::$app->session->set('fb_access_token', $accessToken->getValue());
            return $this->redirect('fb-sign-up');

        } catch(FacebookResponseException $e) {
            throw new BadRequestHttpException('Graph returned an error: ' . $e->getMessage());
        } catch(FacebookSDKException $e) {
            throw new BadRequestHttpException('Facebook SDK returned an error: ' . $e->getMessage());
        }
    }

    /**
     * Getting addition info to FB login
     *
     * @return string
     * @throws BadRequestHttpException
     */
    public function actionFbSignUp()
    {
        try {
           if ($accessToken = \Yii::$app->session->get('fb_access_token')) {
               $fb = $this->getFbInstance();
               $response = $fb->get('/me?fields=id,name,email', $accessToken);
               $fbUserData = $response->getGraphUser();

               $facebookLoginForm = new FacebookLoginForm();
               if ($customerFbLogin = CustomersFbLogin::findOne(['facebook_user_id' => $fbUserData['id']])) {
                   $facebookLoginForm->login($customerFbLogin->customer_id);
                   return $this->redirect('/portal');
               }

               $facebookLoginForm->fbUserId = $fbUserData['id'];
               $facebookLoginForm->name = $fbUserData['name'];
               $facebookLoginForm->email = $fbUserData['email'];

               if ($facebookLoginForm->load(\Yii::$app->request->post()) && $facebookLoginForm->validate()) {
                   if ($customerId = $facebookLoginForm->addNewCustomer()) {
                       $facebookLoginForm->login($customerId);
                       return $this->redirect('/portal');
                   }
               }

               return $this->render('fb-sign-up', [
                   'facebookLoginForm'  => $facebookLoginForm
               ]);
           } else {
               throw new \Exception('Access token is not found');
           }
        }  catch(FacebookResponseException $e) {
            throw new BadRequestHttpException('Graph returned an error: ' . $e->getMessage());
        } catch(FacebookSDKException $e) {
            throw new BadRequestHttpException('Facebook SDK returned an error: ' . $e->getMessage());
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();
        return $this->goBack();
    }

    /**
     * Get FB instance
     *
     * @return Facebook
     * @throws FacebookSDKException
     */
    protected function getFbInstance()
    {
        return new Facebook([
            'app_id'                    => \Yii::$app->params['fb_app_id'],
            'app_secret'                => \Yii::$app->params['fb_app_secret'],
            'default_graph_version'     => \Yii::$app->params['fb_default_graph_version'],
        ]);
    }
}
