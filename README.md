Splynx Add-on Skeleton
======================

Splynx Add-on Skeleton based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-base.git
cd splynx-addon-base
composer global require "fxp/composer-asset-plugin:^1.4.2"
composer install
~~~

Install Splynx Add-on Skeleton:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/VladislavDneprov/splynx.git splynx-addon-fb-login
cd splynx-addon-fb-login
composer install
sudo chmod +x yii
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-addon-fb-login/web/ /var/www/splynx/web/social
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-fb-login.addons
~~~

with following content:
~~~
location /social
{
        try_files $uri $uri/ /social/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

Some settings avaiable in `/var/www/splynx/addons/splynx-addon-fb-login/config/params.php`.

You can then access Splynx Add-On Skeleton in menu "Customers".