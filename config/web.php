<?php

if (!file_exists(__DIR__ . '/../../splynx-addon-base/vendor/splynx/splynx-addon-helpers/helpers/ConfigHelper.php')) {
    exit("Error: Your Add-On Base is very old!\nPlease update your Add-On Base\n");
}

$params = \splynx\helpers\ConfigHelper::getParams(__DIR__);
if ($params == null) {
    die('Error: params.php not found! You must copy end edit example file!');
}

$config = [
    'id' => 'splynx-addon-fb-login',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/../splynx-addon-base/vendor',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'baseUrl' => '/social',
            'enableCookieValidation' => false
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'splynx\models\Customer',
            'idParam' => 'splynx_customer_id',
            'loginUrl' => '/portal/login',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<alias:index>' => 'site/<alias>',
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'sqlite:@app/data/data.db',
            'charset' => 'utf8'
        ],
        'view' => [
            'class' => 'yii\web\View',
            'defaultExtension' => 'twig',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    'cachePath' => '@runtime/Twig/cache',
                    // Array of twig options:
                    'options' => [],
                    'extensions' => [
                        '\splynx\components\twig\TwigActiveForm',
                        '\splynx\components\twig\TwigPjax'
                    ],
                    'globals' => [
                        'Html' => '\yii\helpers\Html',
                        'Url' => '\yii\helpers\Url',
                        'GridView' => '\yii\grid\GridView',
                        'DatePicker' => '\yii\jui\DatePicker',
                        'Yii' => '\Yii',
                    ],
                    'uses' => [
                        'yii\bootstrap'
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['components']['view']['renderers']['twig']['options']['debug'] = true;
    $config['components']['view']['renderers']['twig']['options']['auto_reload'] = true;

    $config['components']['view']['renderers']['twig']['extensions'][] = '\Twig_Extension_Debug';
}

return $config;
