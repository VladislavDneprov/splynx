<?php

if (file_exists(__DIR__ . '/params.php')) {
    $params = require(__DIR__ . '/params.php');
} else {
    $params = null;
}

return [
    'id' => 'splynx-addon-skeleteon-console',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/../splynx-addon-base/vendor',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'sqlite:@app/data/data.db',
            'charset' => 'utf8'
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
