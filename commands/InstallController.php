<?php

namespace app\commands;

use splynx\base\BaseInstallController;

class InstallController extends BaseInstallController
{
    public function getAddOnTitle()
    {
        return 'Splynx Add-on Facebook login';
    }

    public function getModuleName()
    {
        return 'splynx_addon_facebook_login';
    }

    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index', 'add', 'view'],
            ],
            [
                'controller' => 'api\admin\config\Mail',
                'actions' => ['index', 'add'],
            ],
        ];
    }
}
